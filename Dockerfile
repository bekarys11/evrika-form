#Build stage
FROM node:14.17.1-alpine3.13 AS build-stage
USER root
RUN mkdir /app
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

#Production stage
FROM nginx:1.12-alpine AS production-stage
USER root
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 5734
CMD [ "nginx", "-g", "daemon off;" ]



